import Joi from 'joi';

export const contactsUpdateSchema = Joi.object({
    id: Joi.string().required().error(() => 'id is required'),
    name: Joi.string().required().error(() => 'name is required'),
    phone: Joi.number().required().error(() => 'phone is required'),
    subject: Joi.string().required().error(() => 'subject is required'),
    message: Joi.string().required().error(() => 'message is required')
});
