import Joi from 'joi';

export const contactsDeleteSchema = Joi.object({
    id: Joi.string().required().error(() => 'id is required'),
});
