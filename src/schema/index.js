/* ---- Contacts schema -----*/
export {contactsAddSchema} from './contacts/contacts-add';
export {contactsDeleteSchema} from './contacts/contacts-delete';
export {contactsEditSchema} from './contacts/contacts-edit';
export {contactsUpdateSchema} from './contacts/contacts-update';